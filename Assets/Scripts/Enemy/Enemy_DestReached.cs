﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_DestReached : MonoBehaviour {

    private Enemy _enemy;
    private NavMeshAgent _navMeshAgent;

    void SetInitialReferences()
    {
        _enemy = GetComponent<Enemy>();
        if(GetComponent<NavMeshAgent>() != null)
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }
        
    }
    void OnEnable()
    {
        SetInitialReferences();
        _enemy.EventEnemyDie += DisableThis;
    }

    void OnDisable()
    {
        _enemy.EventEnemyDie -= DisableThis;
    }

    void Update()
    {
        if (_enemy.isOnRoute)
        {
            if(_navMeshAgent.remainingDistance < _navMeshAgent.stoppingDistance)
            {
                _enemy.isOnRoute = false;
                _enemy.CallEventEnemyReachedNavTarget();
            }
        }
    }


    void DisableThis()
    {
        this.enabled = false;
    }
}
