﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Attack : MonoBehaviour {

    private Enemy _enemy;
    private Transform _attackTarget;
    private Transform _transform;

    public float attackRate = 1;
    private float nextAttack;
    public float attackRange = 3.5f;
    public int attackDamage = 10;

    private void OnEnable()
    {
        SetInintialReferences();
        _enemy.EventEnemyDie += DisabelThis;
        _enemy.EventEnemySetNavTarget += SetAttackTarget;
    }
    private void OnDisable()
    {
        _enemy.EventEnemyDie -= DisabelThis;
        _enemy.EventEnemySetNavTarget -= SetAttackTarget;
    }

    private void Update()
    {
        TryToAttack();
    }

    void SetInintialReferences()
    {
        _enemy = GetComponent<Enemy>();
        _transform = transform;
    }

    void SetAttackTarget(Transform targetTransform)
    {
        _attackTarget = targetTransform;
    }

    void TryToAttack()
    {
        if(_attackTarget != null)
        {
            if(Time.time > nextAttack)
            {
                nextAttack = Time.time + attackRate;
                if(Vector3.Distance(_transform.position, _attackTarget.position)<= attackRange)
                {
                    Vector3 lookAtVector = new Vector3(_attackTarget.position.x, _transform.position.y, _attackTarget.position.z);
                    _transform.LookAt(lookAtVector);
                    _enemy.CallEventEnemyAttack();
                    _enemy.isOnRoute = false;
                }
            }
        }
    }

    public void OnEnemyAttack()
    {
        if(_attackTarget != null)
        {
            if(Vector3.Distance(_transform.position, _attackTarget.position)<= attackRange && _attackTarget.GetComponent<Player>() != null)
            {
                Vector3 toOther = _attackTarget.position - _transform.position;

                if(Vector3.Dot(toOther, _transform.forward) > 0.5f)
                {
                    _attackTarget.GetComponent<Player>().CallEventPlayerHealthDeduction(attackDamage);
                }
            }
        }
    } 

    void DisabelThis()
    {
        this.enabled = false;
    }
}
