﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_RagdoolActiv : MonoBehaviour {
    private Enemy _enemy;
    private Collider _collider;
    private Rigidbody _rigidbody;

    private void OnEnable()
    {
        SetInintialReferences();
        _enemy.EventEnemyDie += ActivateRagdoll;
    }

    private void OnDisable()
    {
        _enemy.EventEnemyDie -= ActivateRagdoll;
    }
    void SetInintialReferences()
    {
        _enemy = transform.root.GetComponent<Enemy>();
        if(GetComponent<Collider>() != null)
        {
            _collider = GetComponent<Collider>();
        }
        if (GetComponent<Rigidbody>() != null)
        {
            _rigidbody = GetComponent<Rigidbody>();
        }
    }
    void ActivateRagdoll()
    {
        if(_rigidbody != null)
        {
            _rigidbody.isKinematic = false;
            _rigidbody.useGravity = true;
        }

        if(_collider != null)
        {
            _collider.isTrigger = false;
            _collider.enabled = true;
        }
    }
}
