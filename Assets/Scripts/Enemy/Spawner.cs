﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;
    public int numberToSpawn;
    public float proximity;
    private float checkRate;
    private float nextCheck;
    private Transform _transform;
    private Transform playerTransform;
    private Vector3 spawnPositon;

    private void Start()
    {
        SetInintialReferences();
    }
    private void Update()
    {
        CheckDistance();
    }
    void SetInintialReferences()
    {
        _transform = transform;
        playerTransform = GameManager_References._player.transform;
        checkRate = Random.Range(0.8f, 1.2f);
    }
    void CheckDistance()
    {
        if(Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;
            if(Vector3.Distance(_transform.position, playerTransform.position)> proximity)
            {
                SpawnObjects();
                this.enabled = false;
            }
        }
    }

    void SpawnObjects()
    {
        for(int i = 0; i< numberToSpawn; i++)
        {
            spawnPositon = _transform.position + Random.insideUnitSphere * 5;
            Instantiate(objectToSpawn, spawnPositon, _transform.rotation);
        }
    }
}
