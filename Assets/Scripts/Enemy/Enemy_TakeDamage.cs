﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_TakeDamage : MonoBehaviour {

    private Enemy _enemy;
    public int damageMultiplier = 1;
    public bool shouldRemoveCollider;

    private void OnEnable()
    {
        SetInintialReferences();
       // _enemy.EventEnemyDie += RemoveThis;
    }

    private void OnDisable()
    {
       // _enemy.EventEnemyDie -= RemoveThis;
    }
    void SetInintialReferences()
    {
        _enemy = transform.root.GetComponent<Enemy>();
    }
    public void ProcessDamage(int damage)
    {
        int damageToApply = damage * damageMultiplier;
        _enemy.CallEventEnemyDeductHealth(damageToApply);
    }
    void RemoveThis()
    {
        if (shouldRemoveCollider)
        {
            if(GetComponent<Collider>() != null)
            {
                Destroy(GetComponent<Collider>());
            }
            if (GetComponent<Rigidbody>() != null)
            {
                Destroy(GetComponent<Rigidbody>());
            }
        }

        Destroy(this);
    }
}
