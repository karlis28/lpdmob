﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Pause : MonoBehaviour {

    private Enemy _enemy;
    private NavMeshAgent _navMeshAgent;
    private float pauseTime = 1f;

    private void OnEnable()
    {
        SetInintialReferences();
        _enemy.EventEnemyDie += DisableThis;
        _enemy.EventEnemyDeductHealth += PauseNavMeshAgent;
    }
    private void OnDisable()
    {
        _enemy.EventEnemyDie -= DisableThis;
    }
    void SetInintialReferences()
    {
        _enemy = GetComponent<Enemy>();
        if (GetComponent<NavMeshAgent>() != null)
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }
    }
    void PauseNavMeshAgent(int dum)
    {
        if(_navMeshAgent != null)
        {
            if (_navMeshAgent.enabled)
            {
                _navMeshAgent.ResetPath();
                _enemy.isNavPaused = true;
                StartCoroutine(RestartNavMeshAgent());
            }
        }
    }

    IEnumerator RestartNavMeshAgent()
    {
        yield return new WaitForSeconds(pauseTime);
        _enemy.isNavPaused = false;
    }

    void DisableThis()
    {
        StopAllCoroutines();
    }
}
