﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Detection : MonoBehaviour {

    private Enemy enemy;
    private Transform myTransform;
    public Transform head;
    public LayerMask playerLayer;
    public LayerMask sightLayer;
    private float checkRate;
    private float nextCheck;
    private float detectRadius = 80;
    private RaycastHit hit;


    private void OnEnable()
    {
        SetInitialReference();
        enemy.EventEnemyDie += DisableThis;
        
    }

    private void OnDisable()
    {
        enemy.EventEnemyDie -= DisableThis;
    }

    void Update ()
    {
        CarryOutDetection();
	}

    void SetInitialReference()
    {
        enemy = GetComponent<Enemy>();
        myTransform = transform;
        if (head == null)
        {
            head = myTransform;
        }

        checkRate = Random.Range(0.8f, 1.2f);
    }

    void CarryOutDetection()
    {
        if(Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;

            Collider[] colliders = Physics.OverlapSphere(myTransform.position, detectRadius, playerLayer);

            if (colliders.Length > 0)
            {
                foreach(Collider potentialTargetCollider in colliders)
                {
                    if(potentialTargetCollider.CompareTag("Player"))
                    {
                        if (CanPotentialTargetBeSeen(potentialTargetCollider.transform))
                        {
                            break;
                        }
                    }
                }
            }
            else
            {

                enemy.CallEventEnemyLostTarget();
            }

        }
    }

    bool CanPotentialTargetBeSeen(Transform potentialTarget)
    {
        if(Physics.Linecast(head.position, potentialTarget.position, out hit, sightLayer))
        {
            if(hit.transform == potentialTarget)
            {
                enemy.CallEventEnemySetNavTarget(potentialTarget);
                return true;
            }
            else
            {
                enemy.CallEventEnemyLostTarget();
                return false;
            }
        }
        else
        {
            enemy.CallEventEnemyLostTarget();
            return false;
        }
    }

    void DisableThis()
    {
        this.enabled = false;
    }
}
