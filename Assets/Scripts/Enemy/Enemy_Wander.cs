﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Wander : MonoBehaviour {

    private Enemy _enemy;
    private NavMeshAgent _navMeshAgent;
    private Transform _transform;
    private float wanderRange = 10f;
    private NavMeshHit _navHit;
    private Vector3 wanderTarget;

    void SetInitialReferences()
    {
        _enemy = GetComponent<Enemy>();
        if (GetComponent<NavMeshAgent>() != null)
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }
        _transform = transform;
    }
    private void OnEnable()
    {
        SetInitialReferences();
        _enemy.EventEnemyDie += DisableThis;
    }
    private void OnDisable()
    {
        _enemy.EventEnemyDie -= DisableThis;
    }
    // Update is called once per frame
    void Update ()
    {
		if(_enemy.Target == null && !_enemy.isOnRoute && !_enemy.isNavPaused)
        {
            if(RandomWanderTarget(_transform.position, wanderRange, out wanderTarget))
            {
                _navMeshAgent.SetDestination(wanderTarget);
                _enemy.isOnRoute = true;
                _enemy.CallEventEnemyWalking();
            }
        }
	}

    bool RandomWanderTarget(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randPoint = center + Random.insideUnitSphere * wanderRange;
        if(NavMesh.SamplePosition(randPoint, out _navHit, 1.0f, NavMesh.AllAreas))
        {
            result = _navHit.position;
            return true;
        }
        else
        {
            result = center;
            return false;
        }
    }

    void DisableThis()
    {
        this.enabled = false;
    }
}
