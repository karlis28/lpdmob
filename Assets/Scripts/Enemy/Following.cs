﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Following : MonoBehaviour {

    [Range(0f, 10f)]
    [SerializeField]
    private float closeEnoughDistance = 1f;
    private Enemy _enemy;

    private GameObject _player;
    private NavMeshAgent _navMeshAgent;

    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player"); //Find player by tag. It can be assigned in game object inspector tag section (below name).
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _enemy = GetComponent<Enemy>();
    }

    void Update()
    {
        if(_enemy.Target != null && _navMeshAgent != null && !_enemy.isNavPaused)
        {
            if (Vector3.Distance(transform.position, _player.transform.position) > closeEnoughDistance) // check if distance between player and gameobject is greater than close enough value
            {
                PerformFollowPlayer();
                if (_navMeshAgent.remainingDistance > _navMeshAgent.stoppingDistance)
                {
                    _enemy.CallEventEnemyWalking();
                    _enemy.isOnRoute = true;
                }
            }
        }

    }

    /// <summary>
    /// Follows Player
    /// </summary>
    private void PerformFollowPlayer()
    {
        _navMeshAgent.SetDestination(_player.transform.position);
    }
}
