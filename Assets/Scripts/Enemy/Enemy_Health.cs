﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Health : MonoBehaviour {

    private Enemy _enemy;
    public int _enemyHealth = 100;
    private void OnEnable()
    {
        SetInintialReferences();
        _enemy.EventEnemyDeductHealth += DeductHealth;
    }
    private void OnDisable()
    {
        _enemy.EventEnemyDeductHealth -= DeductHealth;
    }
    void DeductHealth(int healthChange)
    {
        _enemyHealth -= healthChange;
        if(_enemyHealth <= 0)
        {
            _enemyHealth = 0;
            _enemy.CallEventEnemyDie();
            Destroy(gameObject, Random.Range(10, 20));
        }
    }
    void SetInintialReferences()
    {
        _enemy = GetComponent<Enemy>();
    }
}
