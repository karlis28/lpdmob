﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Animation : MonoBehaviour {

    private Enemy enemy;
    private Animator animator;
    
	void OnEnable ()
    {
        SetInitialReference();
        enemy.EventEnemyDie += DisableAnimator;
        enemy.EventEnemyWalking += SetAnimationToWalk;
        enemy.EventEnemyReachedNavTarget += SetAnimationToIdle;
        enemy.EventEnemyAttack += SetAnimationToAttack;
        enemy.EventEnemyDeductHealth += SetAnimationToStruck;
    }

	
	void OnDisable ()
    {
        enemy.EventEnemyDie -= DisableAnimator;
        enemy.EventEnemyWalking -= SetAnimationToWalk;
        enemy.EventEnemyReachedNavTarget -= SetAnimationToIdle;
        enemy.EventEnemyAttack -= SetAnimationToAttack;
        enemy.EventEnemyDeductHealth -= SetAnimationToStruck;
    }

    void SetInitialReference()
    {
        enemy = GetComponent<Enemy>();

        if(GetComponent<Animator>() != null)
        {
            animator = GetComponent<Animator>();
        }
    }
    void SetAnimationToIdle()
    {
        if (animator != null)
        {
            if (animator.enabled)
            {
                animator.SetBool("isPursuing", false);
            }
        }
    }
    void SetAnimationToWalk()
    {
        if (animator != null)
        {
            if (animator.enabled)
            {
                animator.SetBool("isPursuing", true);
            }
        }
    }
    void SetAnimationToAttack()
    {
        if (animator != null)
        {
            if (animator.enabled)
            {
                animator.SetTrigger("Attack");
            }
        }
    }
    void SetAnimationToStruck(int dummy)
    {
        if (animator != null)
        {
            if (animator.enabled)
            {
                animator.SetTrigger("Struck");
            }
        }
    }

    void DisableAnimator()
    {
        if(animator != null)
        {
            animator.enabled = false;
        }
    }
}
