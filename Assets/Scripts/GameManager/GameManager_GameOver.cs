﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager_GameOver : MonoBehaviour {

    private GameManager _gameManager;
    public GameObject panelGameOver;

    private void OnEnable()
    {
        SetInitialReferences();
        _gameManager.GameOverEvent += TurnOnGameOverPanel;
    }
    private void OnDisable()
    {
        _gameManager.GameOverEvent -= TurnOnGameOverPanel;
    }
    void SetInitialReferences()
    {
        _gameManager = GetComponent<GameManager>();
    }
    void TurnOnGameOverPanel()
    {
        if(panelGameOver != null)
        {
            panelGameOver.SetActive(true);
        }
    }
}
