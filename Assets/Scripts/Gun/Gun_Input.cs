﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Input : MonoBehaviour {

    private Gun _gun;
    private float nextAttack;
    public float attackRate = 0.5f;
    private Transform _transform;
    public string attackButtonName;

    private void Start()
    {
        SetInitialReferences();
    }

    private void Update()
    {
        CheckIfWeponShouldAttack();
    }
    
    void SetInitialReferences()
    {
        _gun = GetComponent<Gun>();
        _transform = transform;
        
    }
    
    void CheckIfWeponShouldAttack()
    {
        if(Time.time > nextAttack && Time.timeScale >0 && _transform.root.CompareTag(GameManager_References._playerTag))
        {
            if (Input.GetButton(attackButtonName))
            {
                AttemptAttack();
            }
        }
    }

    void AttemptAttack()
    {
        nextAttack = Time.time + attackRate;
        _gun.CallEventPlayerInput();
    }

    
}
