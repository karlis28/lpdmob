﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Damage : MonoBehaviour {
    private Gun _gun;
    public int damage = 10;

    private void OnEnable()
    {
        setInitialReferences();
        _gun.EventShotEnemy += ApplyDamage;
        
    }

    private void OnDisable()
    {
        _gun.EventShotEnemy -= ApplyDamage;
    }

    void setInitialReferences()
    {
        _gun = GetComponent<Gun>();
    }

    void ApplyDamage(Vector3 hitPosition, Transform hitTransform)
    {
        
        if (hitTransform.GetComponent<Enemy_TakeDamage>() != null)
        {
            hitTransform.GetComponent<Enemy_TakeDamage>().ProcessDamage(damage);
            
        }
    }
}
