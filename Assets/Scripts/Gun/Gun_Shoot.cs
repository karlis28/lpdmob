﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Shoot : MonoBehaviour {
    private Gun _gun;
    private Transform _transform;
    private Transform camTransform;
    private RaycastHit hit;
    public float range = 400;
    private float offsetFactor = 7;
    private Vector3 startPosition;

    private void OnEnable()
    {
        SetInitialReferences();
        _gun.EventPlayerInput += OpenFire;
        _gun.EventSpeedCaptured += SetStartOfShootingPosition;
    }

    private void OnDisable()
    {
        _gun.EventPlayerInput -= OpenFire;
    }

    void SetInitialReferences()
    {
        _gun = GetComponent<Gun>();
        _transform = transform;
        camTransform = _transform.parent;
    }

    void OpenFire()
    {
        if (Physics.Raycast(camTransform.TransformPoint(startPosition), camTransform.forward, out hit, range))
        {
            _gun.CallEventShotDefault(hit.point, hit.transform);

            if (hit.transform.CompareTag(GameManager_References._enemyTag))
            {
                _gun.CallEventShotEnemy(hit.point, hit.transform);
            }
        }
    }

    void SetStartOfShootingPosition(float playerSpeed)
    {
        float offset = playerSpeed / offsetFactor;
        startPosition = new Vector3(Random.Range(-offset, offset), Random.Range(-offset, offset), 1);
    }
}
