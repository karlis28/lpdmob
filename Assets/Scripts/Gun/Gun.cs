﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public delegate void GeneralEventHandler();
    public event GeneralEventHandler EventPlayerInput;

    public delegate void GunHitEventHandler(Vector3 hitPosition, Transform hitTransform);
    public event GunHitEventHandler EventShotDefault;
    public event GunHitEventHandler EventShotEnemy;

    public delegate void GunCrosshairEventHandler(float speed);
    public event GunCrosshairEventHandler EventSpeedCaptured;
    public void CallEventPlayerInput()
    {
        if(EventPlayerInput != null)
        {
            EventPlayerInput();
        }
    }

    public void CallEventShotDefault(Vector3 hPos, Transform hTransform)
    {
        if (EventShotDefault != null)
        {
            EventShotDefault(hPos, hTransform);
        }
    }

    public void CallEventShotEnemy(Vector3 hPos, Transform hTransform)
    {
        
        if (EventShotEnemy != null)
        {
            EventShotEnemy(hPos, hTransform);
        }
    }

    public void CallEventSpeedCaptured(float spd)
    {
        if (EventSpeedCaptured != null)
        {
            EventSpeedCaptured(spd);
        }
    }
}
