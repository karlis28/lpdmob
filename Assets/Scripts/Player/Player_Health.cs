﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Health : MonoBehaviour {

    private GameManager _gameManager;
    private Player _player;

    public int playerHealth;
    public Text healthText;

    private void OnEnable()
    {
        SetInitialReferences();
        SetUI();
        _player.EventPlayerHealthDeduction += DeductHealth;
    }
    public void OnDisable()
    {
        _player.EventPlayerHealthDeduction -= DeductHealth;
    }

    void DeductHealth(int healthChange)
    {
        playerHealth -= healthChange;
        if(playerHealth <= 0)
        {
            playerHealth = 0;
            _gameManager.CallEventGameOver();
        }

        SetUI();
    }

    void SetInitialReferences()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _player = GetComponent<Player>();
    }

    void SetUI()
    {
        if(healthText != null)
        {
            healthText.text = playerHealth.ToString();
        }
    }

}
