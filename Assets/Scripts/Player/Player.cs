﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public delegate void GeneralEventHandler();
    public event GeneralEventHandler EventAmmoChanged;

    public delegate void AmmoPickupEventHandler(string ammoName, int quantity);
    public event AmmoPickupEventHandler EventPickedUpAmmo;

    public delegate void PlayerHealthEventHandler(int healthChange);
    public event PlayerHealthEventHandler EventPlayerHealthDeduction;

    public void CallEventAmmoChanged()
    {
        if(EventAmmoChanged != null)
        {
            EventAmmoChanged();
        }
    }
    public void CallEventPickedUpAmmo(string ammoName, int quantity)
    {
        if (EventPickedUpAmmo != null)
        {
            EventPickedUpAmmo(ammoName, quantity);
        }
    }
    public void CallEventPlayerHealthDeduction(int dmg)
    {
        if(EventPlayerHealthDeduction != null)
        {
            EventPlayerHealthDeduction(dmg);
        }
    }
}
