﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitHydrant : MonoBehaviour {

    private Transform _transform;
    private GameObject _player;

    private void OnEnable()
    {
        SetInitialReferences();
    }

    void SetInitialReferences()
    {
        _transform = transform;
        _player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            _transform.position = Vector3.Lerp(_transform.position, _transform.position + _player.transform.position / 100, Time.deltaTime/60);
        }


    }
}
